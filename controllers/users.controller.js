const passport = require("passport");
const User = require('../models/User');


module.exports = {
  usersGet: async (req, res, next) => {
    try {
      const users = await User.find();
      return res.render('users', { users })
    } catch(error) {
      return next(error);
    }
  },

  registerPost: (req, res, next) => {
    passport.authenticate("register", (error, user) => {
      if (error) {
        return res.render("register", { error: error.message });
      }

      req.logIn(user, (error) => {
        if (error) {
          return res.render("register", { error: error.message });
        }

        return res.redirect("/products");
      });
    })(req, res, next);
  },

  loginPost: (req, res, next) => {
    passport.authenticate("login", (error, user) => {
      if (error) {
        return res.render("login", { error: error.message });
      }

      req.logIn(user, (error) => {
        if (error) {
          return res.render("login", { error: error.message });
        }

        return res.redirect("/products");
      });
    })(req, res, next);
  },

  logoutPost: (req, res, next) => {
    if (req.user) {
      req.logout();

      req.session.destroy(() => {
        res.clearCookie("connect.sid");

        return res.redirect("/");
      });
    } else {
      return res.sendStatus(304);
    }
  },
};