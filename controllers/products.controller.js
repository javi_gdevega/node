const Product = require('../models/Product');

const controller = {};

controller.indexGet = async(req, res) => {
    try {
        const products = await Product.find();
        
        return res.status(200).render('products', { 
            title: 'Motos de alquiler', 
            products,
            isAdmin: req.user.role === "admin",
         }) 
    } catch (error) {
        return res.status(500).json(error);
    }
};

controller.createGet = (req, res, next) => {
    res.status(200).render("create-product");
};

controller.createPost = async(req, res, next) => {
    try {
        const {
            brand,
            model,
            type,
            power,
            weight,
            price,
            image,
          } = req.body;

        const newProduct = new Product({
            brand,
            model,
            type,
            power: Number(power),
            weight: Number(weight),
            price: Number(price),
            image: req.file.url
        });

        const createdProduct = await newProduct.save();

        return res.status(201).redirect(`/products/${createdProduct._id}`);

    } catch (error) {
        next(error);
    }
};

controller.getByIdGet = async (req, res, next) => {
    const id = req.params.id;
  
    try {
      let productFinded = await Product.findById(id);
  
      if(!productFinded) {
        productFinded = false;
      }
  
      return res.status(200).render('product', { title: `${productFinded.model || 'No Product Found'} Page`, product: productFinded, id });
  
    } catch (error) {
      next(error);
    }
};

controller.getByPriceGet =  async(req, res) => {
    const price = req.params.price;

    try {
        const productsByPrice = await Product.find({ price: { $price: price }});
        return res.status(200).json(productsByPrice);
    } catch (err) {
        return res.status(500).json(err);
    }
};

controller.editPut = async (req, res, next) => {
    try {
      const { model, id } = req.body;
  
      const updatedProduct = await Product.findByIdAndUpdate(
        id,
        { model: model },
        { new: true }
      );
      return res.status(200).json(updatedProduct);
    } catch (error) {
      next(error);
    }
};

controller.deleteGet = async (req, res, next) => {
    const { id } = req.params;
  
    try {
      await Product.findByIdAndDelete(id);
      res.redirect("/products");
    } catch (error) {
      next(error);
    }
};
  



  module.exports = controller;