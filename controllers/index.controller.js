const indexGet = (req, res, next) => {
    return res.status(200).render("index", { title: "Motorland - Rent a bike" });
}

const registerGet = (req, res, next) => {
  return res.render("register");
};

const loginGet = (req, res, next) => {
  return res.render("login");
};

module.exports = {
    indexGet,
    registerGet,
    loginGet
}