const mongoose = require('mongoose');
require('dotenv').config();

const DB_URL = 'mongodb://localhost:27017/proyecto';

const connect = () => {

    mongoose.connect(DB_URL, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    })
    .then(() => {
        console.log(`Sucess connected to DB`)
    })
    .catch((err) => {
        console.log(`Error connecting to DB ${err}`)
    })
}
mongoose.connect(DB_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
})
.then(() => {
    console.log(`Sucess connected to DB`)
})
.catch((err) => {
    console.log(`Error connecting to DB ${err}`)
})

module.exports = { DB_URL, connect };