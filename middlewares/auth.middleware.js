const isAuthenticated = (req, res, next) => {
    if(req.isAuthenticated()) {
      return next();  
    } else {
        return res.redirect('/login');
    }
};

/**
 * Middleware para comprobar si nuestro usuario es administrador.
 */
const isAdmin = (req, res, next) => {
    console.log(req.passport);
    if(req.user.role === 'admin') {
        return next();
    } else {
        return res.redirect('/');
    }
}

module.exports = {
    isAuthenticated,
    isAdmin,
}