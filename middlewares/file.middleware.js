const multer = require('multer');
const fs = require('fs');
require('dotenv').config();
const cloudinary = require("cloudinary").v2;
const path = require('path');

/**
 * Este es nuestro middleware para gestionar la subida de archivos. Nos ayudará multer,
 * que se encarga de recibir el archivo desde req.text y transformarlo al formato que deseemos.
 * En este caso imágenes.
 * 
 * Un middleware no es más que una función que se ejecuta antes del controlador. Si
 * esta función llama a next(), se ejecutará el controlador pero si no llama a next(), el servidor
 * hará el comportamiento que nosotros programemos
 */

/**
 * Configuramos un array como 'variable global' en el que almacenamos dos strings con los tipos
 * de archivo que vamos a permitir subir al servidor.
 */
const VALID_FILE_TYPES = ['image/png', 'image/jpg', 'image/jpeg'];

/**
 * Configuramos nuestro almacenamiento con el método multer.diskStorage (consultar documentación de multer)
 * Para ello, elegimos el nombre del archivo y el lugar donde se va a almacenar.
 */
const storage = multer.diskStorage({
    filename: (req, file, cb) => {
        cb(null, `${Date.now()}-${file.originalname}`);
    },
    destination: (req, file, cb) => {
        cb(null, path.join(__dirname, '../public/uploads'))
    }
});

/**
 * Función que vamos a ejecutar antes de guardar nuestro archivo. 
 * En nuestro caso, validaremos que el tipo de archivo que está subiendo el usuario,
 * está permitido.
 */
const fileFilter = (req, file, cb) => {
    if(!VALID_FILE_TYPES.includes(file.mimetype)) {
        const error = new Error('Invalid file type');
        cb(error, false);
    } else {
        cb(null, true);
    }
}

/**
 * Guardarmos la invocación de multer y su configuración en una constante y exportamos
 * la constante para poder usarla desde la llamada a un middleware.
 */
const upload = multer({
  storage,
  fileFilter,
  limits: {
    fieldNameSize: 500,
    fieldSize: 500000000,
  },
});


/**
 * Middleware para subir nuestros archivos a Cloudinary
 * un servicio de terceros donde almacenaremos las imágenes
 * que el usuario suba a nuestro servidor
 */
const uploadToCloudinary = async(req, res, next) => {    
    if(req.file) {
        const filePath = req.file.path;
        const image = await cloudinary.uploader.upload(filePath);
        /**
         * Primero subimos la imagen a nuestro servidor con multer
         * Una vez subida, la enviamos a Cloudinary y este nos devuelve
         * la url de nuestra imagen. Cuando obtenemos la url, borramos
         * la imagen de nuestro servidor.
         */
        await fs.unlinkSync(filePath);

        req.file_url = image.secure_url;

        /**
         * Continuamos ejecutando nuestro controller
         */
        return next();
    } else {
        return next();
    }
}

module.exports = { upload, uploadToCloudinary };