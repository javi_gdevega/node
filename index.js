const express = require('express');
const path = require('path');
const passport = require('passport');
const mongoose = require('mongoose');
const session = require('express-session');
const MongoStore = require('connect-mongo')(session);
const indexRoutes = require('./routes/index.routes');
const productRoutes = require('./routes/products.routes');
const userRoutes = require('./routes/user.routes');

require('dotenv').config();

require('./helpers/hbs.helpers');

const db = require('./db.js');
db.connect();

require('./passport');

require('dotenv').config();


const PORT = process.env.PORT || 3000;
const server = express();

server.set('views', path.join(__dirname, 'views'));
server.set('view engine', 'hbs');


server.use(express.json());
server.use(express.urlencoded({ extended: false }));
server.use(express.static(path.join(__dirname, 'public')))

server.use(
    session({
      secret: 'motorland',
      resave: false,
      saveUninitialized: false,
      cookie: {
        maxAge: 360000,
      },
      store: new MongoStore({ mongooseConnection: mongoose.connection })
    })
  );

server.use(passport.initialize());
server.use(passport.session());

server.use('/', indexRoutes)
server.use('/products', productRoutes);
server.use('/users', userRoutes);

server.use('*', (req, res, next) => {
    const error = new Error('Route not found');
    error.status = 404;
    next(error);
});

server.use((err, req, res, next) => {
    return res.status(err.status || 500).render('error' , {
        message: err.message || 'Unexpected error',
        status: err.status || 500,
    });
});

server.listen(PORT, () => {
    console.log(`Server running in http://localhost:${PORT}`)
});