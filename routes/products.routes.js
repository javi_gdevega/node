const express = require('express');
const Product = require('../models/Product');

const authMiddleware = require('../middlewares/auth.middleware');
const fileMiddleware = require('../middlewares/file.middleware');

const productsController = require('../controllers/products.controller');

const router = express.Router();


router.get("/", [authMiddleware.isAuthenticated], productsController.indexGet);
router.get("/create", [authMiddleware.isAuthenticated], productsController.createGet);
router.post('/create', 
[fileMiddleware.upload.single("image"), fileMiddleware.uploadToCloudinary], productsController.createPost);
router.get("/:id", [authMiddleware.isAuthenticated], productsController.getByIdGet);
router.get('/price/:price', [authMiddleware.isAuthenticated], productsController.getByPriceGet);
router.put("/edit", [authMiddleware.isAuthenticated], productsController.editPut);
router.get("/:id/delete", [authMiddleware.isAuthenticated], productsController.deleteGet);


module.exports = router;