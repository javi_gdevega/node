const express = require('express');
const authMiddleware = require('../middlewares/auth.middleware');
const usersController = require('../controllers/users.controller');

const router = express.Router();

router.get("/", [authMiddleware.isAuthenticated, authMiddleware.isAdmin], usersController.usersGet);
router.post('/register', usersController.registerPost);
router.post('/login', usersController.loginPost);
router.post('/logout', usersController.logoutPost)

module.exports = router;