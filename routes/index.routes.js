const express = require('express');

const indexController = require('../controllers/index.controller');
const { route } = require('./user.routes');

const router = express.Router();

router.get("/", indexController.indexGet);
router.get('/register', indexController.registerGet);
router.get('/login', indexController.loginGet);

module.exports = router;