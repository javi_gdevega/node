const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const productSchema = new Schema(
    {
        brand: { 
            type: String, 
            required: true
        },
        model: { 
            type: String, 
            required: true
        },
        type: {
            enum: ['Sport', 'Naked', 'Scooter', 'Trail'], 
            type: String, 
            required: true
        },
        power: { 
            type: Number, 
            required: true
        },
        weight: {
            type: Number,
            required: true,
        },
        price: {
            type: Number,
            required: true
        },
        image: {
            type: String,
        }
    },
    {
        timestamps: true,
    }
);

const Product = mongoose.model('Product', productSchema);
module.exports = Product;