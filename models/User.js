const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const userSchema = new Schema(
    {
        email: { 
            type: String, 
            required: true
        },
        password: { 
            type: String, 
            required: true
        },
        cart: [{
            type: mongoose.Types.ObjectId,
            ref: 'Products'
        }]
    },
    {
        timestamps: true,
    }
);

const User = mongoose.model('User', userSchema);
module.exports = User;