const mongoose = require('mongoose');
const DB_URL = require('../db');
const Product = require('../models/Product');

const products = [
    {
       brand: "Yamaha",
       model: "Tracer 900",
       type: "Trail",
       power: 119,
       weight: 210,
       price: 90,
       image: "https://www.motofichas.com/images/phocagallery/Yamaha_Motor_Corporation/tracer-9-2021/01-yamaha-tracer-9-2021-estudio-rojo.jpg",
    },
    {
        brand: "Yamaha",
        model: "Tracer 700",
        type: "Trail",
        power: 74,
        weight: 185,
        price: 80,
        image: "https://www.motofichas.com/images/phocagallery/Yamaha_Motor_Corporation/tracer-7-2021/01-yamaha-tracer-7-2021-estudio-rojo.jpg",
     },
     {
        brand: "Yamaha",
        model: "T-Max 530",
        type: "Scooter",
        power: 46,
        weight: 213,
        price: 70,
        image: "https://www.motofichas.com/images/phocagallery/Yamaha_Motor_Corporation/tmax-tech-max-2020/02-yamaha-tmax-2021-tech-max-estudio-gris.jpg",
     },
     {
        brand: "Yamaha",
        model: "YZF-R1",
        type: "Sport",
        power: 200,
        weight: 200,
        price: 120,
        image: "https://www.motofichas.com/images/phocagallery/Yamaha_Motor_Corporation/yzf-r1-m-2020/01-yamaha-yzf-r1-2020-perfil-azul.jpg",
     },
     {
        brand: "BMW",
        model: "R 1250 GS",
        type: "Trail",
        power: 125,
        weight: 245,
        price: 130,
        image: "https://www.motofichas.com/images/phocagallery/BMW_Motorrad/r-1250-gs-2021/04-bmw-r-1250-gs-2020-estudio-rallye.jpg",
     },
     {
        brand: "BMW",
        model: "S 1000 RR",
        type: "Sport",
        power: 207,
        weight: 180,
        price: 140,
        image: "https://www.motofichas.com/images/phocagallery/BMW_Motorrad/s-1000-rr-2021/03-bmw-s-1000-rr-2019-estatica.jpg",
     },
     {
        brand: "BMW",
        model: "F 900 XR",
        type: "Trail",
        power: 105,
        weight: 215,
        price: 120,
        image: "https://www.motofichas.com/images/phocagallery/BMW_Motorrad/f-900-xr-2020/01-bmw-f-900-xr-2020-esudio-amarillo.jpg",
     },
     {
        brand: "BMW",
        model: "C 650 Sport",
        type: "Scooter",
        power: 60,
        weight: 225,
        price: 90,
        image: "https://www.motofichas.com/images/phocagallery/BMW_Motorrad/c-650-sport-2018/01-bmw-c-650-sport-2020-azul-perfil.jpg",
     },
     {
        brand: "Honda",
        model: "Forza 750",
        type: "Scooter",
        power: 59,
        weight: 235,
        price: 60,
        image: "https://www.motofichas.com/images/phocagallery/Honda/forza-750/01-honda-forza-750-estudio-azul.jpg",
     },
     {
        brand: "Honda",
        model: "CB 1000 R",
        type: "Naked",
        power: 143,
        weight: 212,
        price: 125,
        image: "https://www.motofichas.com/images/phocagallery/Honda/cb1000r-2021/01-honda-cb1000r-2021-estudio-negro.jpg",
     },
     {
        brand: "Honda",
        model: "Afica Twin",
        type: "Trail",
        power: 102,
        weight: 226,
        price: 130,
        image: "https://www.motofichas.com/images/phocagallery/Honda/crf1100l-africa-twin-adventure-sports-2020/01-honda-crf1100l-africa-twin-2020-estudio-rojo.jpg",
     },
     {
        brand: "Triumph",
        model: "Tiger 900 GT",
        type: "Trail",
        power: 95,
        weight: 192,
        price: 115,
        image: "https://www.motofichas.com/images/phocagallery/Triumph_Motorcycles/tiger-900-gt-pro-2020/01-triumph-tiger-900-2020-estatica-blanco-pure-white.jpg",
     },
     {
        brand: "Triumph",
        model: "Street Triple RS",
        type: "Naked",
        power: 123,
        weight: 166,
        price: 125,
        image: "https://www.motofichas.com/images/phocagallery/Triumph_Motorcycles/street-triple-rs-2020/01-triumph-street-triple-rs-2020-estudio.jpg",
     },
     {
        brand: "Triumph",
        model: "Speed Triple RS",
        type: "Naked",
        power: 150,
        weight: 192,
        price: 140,
        image: "https://www.motofichas.com/images/phocagallery/Triumph_Motorcycles/speed-triple-s-rs-2018/01-triumph-speed-triple-s-2018-perfil-blanco.jpg",
     },
     {
        brand: "KTM",
        model: "1290 Super Adventure",
        type: "Trail",
        power: 160,
        weight: 215,
        price: 150,
        image: "https://www.motofichas.com/images/phocagallery/KTM/1290_Super_Adventure_S_2017/01-ktm-1290-adventure-s-2019-estatica-naranja.jpg",
     },
     {
        brand: "KTM",
        model: "690 SMC R",
        type: "Trail",
        power: 75,
        weight: 147, 
        price: 95,
        image: "https://www.motofichas.com/images/phocagallery/KTM/690-smc-r-2019/02-ktm-690-smc-r-2019-perfil.jpg",
     },
     {
        brand: "KTM",
        model: "1290 Suoer Duke GT",
        type: "Naked",
        power: 175,
        weight: 212,
        price: 145, 
        image: "https://www.motofichas.com/images/phocagallery/KTM/1290-super-duke-gt-2019/01-ktm-1290-super-duke-gt-2019-perfil-blanco.jpg",
     },
     {
        brand: "Kawasaki",
        model: "Z1000 R",
        type: "Naked",
        power: 142,
        weight: 221,
        price: 110, 
        image: "https://www.motofichas.com/images/phocagallery/Kawasaki/z1000-r-2019/01-kawasaki-z1000-2019-estatica.jpg",
     },
     {
        brand: "Kawasaki",
        model: "Versys 1000",
        type: "Trail",
        power: 120,
        weight: 239,
        price: 130, 
        image: "https://www.motofichas.com/images/phocagallery/Kawasaki/versys-1000-2021/01-kawasaki-versys-1000-se-2021-estudio-verde.jpg",
     },
     {
        brand: "Kawasaki",
        model: "Z 900RS",
        type: "Naked",
        power: 111,
        weight: 239,
        price: 100, 
        image: "https://www.motofichas.com/images/phocagallery/Kawasaki/z900rs-cafe-2020/01-kawasaki-z900rs-2020-estudio.jpg",
     },
];

mongoose.connect(DB_URL, {
   useNewUrlParser: true,
   useUnifiedTopology: true,
})
.then(async () => {
   const allProducts = await Product.find();

   if(allProducts.length) {
       await Product.collection.drop();
   }
})
.catch((err) => {
   console.log(`Error deleting db data ${err}`);
})
.then(async () => {
   await Product.insertMany(products);
})
.catch((err) => {
   console.log(`Error adding data to our db ${err}`)
})
.finally(() => mongoose.disconnect());
