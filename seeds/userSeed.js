const mongoose = require('mongoose');
const DB_URL = require('../db');
const User = require('../models/User');

const users = [
    {
       email: "javi@gmail.com",
       password: "1234",
    },
];

mongoose.connect(DB_URL, {
   useNewUrlParser: true,
   useUnifiedTopology: true,
})
.then(async () => {
   const allUsers = await User.find();

   if(allUsers.length) {
       await User.collection.drop();
   }
})
.catch((err) => {
   console.log(`Error deleting db data ${err}`);
})
.then(async () => {
   await User.insertMany(users);
})
.catch((err) => {
   console.log(`Error adding data to our db ${err}`)
})
.finally(() => mongoose.disconnect());
