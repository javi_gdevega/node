const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const bcrypt = require('bcrypt');
const User = require('./models/User');

const saltRound = 10;

const validate = (email) => {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  };

// Se activan con req.logIn
passport.serializeUser((user, done) => {
    return done(null, user._id);
});

passport.deserializeUser(async (userId, done) => {
    try {
        const existingUser = await User.findById(userId);
        return done(null, existingUser);
    } catch (err) {
        return done(err);
    }
});

passport.use(
    'register',
    new LocalStrategy(
        {
            usernameField: 'email',
            passwordField: 'password',
            passReqToCallback: true,
        },
        async (req, email, password, done) => {
            try {
                if(email.length < 6) {
                    const error = new Error('Email must be 6 characters min');
                    return done(error);
                }

                const validEmail = validate(email);

                if (!validEmail) {
                    const error = new Error("Invalid Email");
                    return done(error);
                }

                // Buscamos si existe el usuario.
                const previousUser = await User.findOne({ email: email.toLowerCase() });

                // Si el usuario existe, nuestro código sale aquí y devuelve un error
                if (previousUser) {
                  const error = new Error("The user already exists");
                  return done(error);
                }

                // Si el usuario no existe, continuamos ejecutando.
                // En esta línea encriptamos la contraseña del usuario antes de 
                // almacenarla en nuestra base de datos.
                const hash = await bcrypt.hash(password, saltRound);

                // Creamos el nuevo usuario en base al modelo
                const newUser = new User({
                  email: email.toLowerCase(),
                  name: req.body.name,
                  password: hash,
                });
                
                // Recuperamos el usuario guardado
                const savedUser = await newUser.save();

                // Devolvemos el callback con el usuario guardado
                return done(null, savedUser);
            } catch(err) {

                // Ejecutamos callback pasándole error.
                return done(err);
            }
        }
    )
);

passport.use(
    'login',
    new LocalStrategy(
        {
            usernameField: 'email',
            passwordField: 'password',
            passReqToCallback: true,
        },
        async (req, email, password, done) => {
            try {
                const validEmail = validate(email);

                if (!validEmail) {
                  const error = new Error("Invalid Email");
                  return done(error);
                }
                
                const currentUser = await User.findOne({ email: email.toLowerCase() });

                if(!currentUser) {
                    const error = new Error('The user does not exist!');
                    return done(error);
                }

                const isValidPassword = await bcrypt.compare(
                    password,
                    currentUser.password
                );

                if (!isValidPassword) {
                    const error = new Error('The email or password is invalid!');
                    return done(error);
                }

                return done(null, currentUser);

            } catch (err) {
                return done(err);
            }
        }
    )
);