const hbs = require('hbs');

hbs.registerHelper("gte", (a, b, options) => {
  if (a >= b) {
    return options.fn(this);
  } else {
    return options.inverse(this);
  }
});

hbs.registerHelper("uppercase", (str) => {
  return str.toUpperCase();
});

